﻿namespace MVCAPI.Migrations.EmployeeDB
{
    using MVCAPI.Data;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MVCAPI.Data.EmployeeDB>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Migrations\EmployeeDB";
        }

        protected override void Seed(MVCAPI.Data.EmployeeDB context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
            context.Employees.AddOrUpdate(
             e => new { e.EmployeeName, e.EmployeeSalary, e.EmployeeAge,e.EmployeeImage }, Dummy.GetEmployees(context).ToArray());
        }
    }
}
