﻿


First Scenario

1. Go to Dummy.cs file in "Data" folder to view the employee details or to add new employees.

2. Two employees are added by default, their details are in Dummy.cs file, available in Data Folder for sign in purpose.

3. Run the project and you will be able to view all the API calls for the Employees.

4.  Average Age and Average Salary Button share single text box

6. Response result is displayed as an Json.


NOTE: IF THE ABOVE SCENARIO DOES NOT WORK THEN PLEASE FOLLOW THE STEPS MENTIONED IN SECOND SCENARIO BELOW FOR SETTING UP YOUR DATABASE.
---------------------------------------------------------------------------------------------------------------------------------------------------------------


Second Scenario

1. Select the "APP_Data" folder and click show all files option through solution explorer.

2. Delete the existing databases.

3. Delete the folder Migrations if exists.

4. Open Migrations.txt file in Docs folder and execute all three commands in a sequence mentioned in the file.

5. Two employees are added by default, their details are in Dummy.cs file, available in Data Folder.

6. Run the project and you will be able to view all the API calls for the Employees.

7.  Average Age and Average Salary Button share single text box

8. Response result is displayed as an Json.

