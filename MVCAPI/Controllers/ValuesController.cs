﻿using Microsoft.Ajax.Utilities;
using MVCAPI.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace MVCAPI.Controllers
{
    public class ValuesController : ApiController
    {

        EmployeeDB db = new EmployeeDB();
        // GET api/values
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/values/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/values
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT api/values/5
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/values/5
        //public void Delete(int id)
        //{
        //}

        [HttpGet]
        public HttpResponseMessage GetAllEmployee()
        {
            try
            {
                var result = new HttpResponseMessage(HttpStatusCode.OK);
                var member = db.Employees.ToList();
                result.Content = new StringContent(JsonConvert.SerializeObject(member));
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return result;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        public HttpResponseMessage GetEmployeeByID(int EmployeeId)
        {
            try
            {
                var result = new HttpResponseMessage(HttpStatusCode.OK);
                var member = db.Employees.Where(x => x.EmployeeId == EmployeeId).FirstOrDefault();
                result.Content = new StringContent(JsonConvert.SerializeObject(member));
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return result;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        public HttpResponseMessage GetCompanyAvgAge()
        {
            try
            {
                var result = new HttpResponseMessage(HttpStatusCode.OK);
                var data = db.Employees.Select(x => x.EmployeeAge).ToList();
                decimal age = 0;

                if (data.Count > 0)
                {
                    age = data.AsQueryable().Sum();
                    age = Math.Ceiling(age / data.Count());
                }

                var obj = new
                {
                    AverageAgeInCompany = age
                };

                result.Content = new StringContent(JsonConvert.SerializeObject(obj));
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return result;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        public HttpResponseMessage GetCompanyAvgSalary()
        {
            try
            {
                var result = new HttpResponseMessage(HttpStatusCode.OK);
                var data = db.Employees.Select(x => x.EmployeeSalary).ToList();
                double salary = 0;

                if (data.Count > 0)
                {
                    salary = data.AsQueryable().Sum();
                    salary = Math.Ceiling(salary / data.Count());
                }

                var obj = new
                {
                    AverageSalaryInCompany = salary
                };

                result.Content = new StringContent(JsonConvert.SerializeObject(obj));
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return result;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        public HttpResponseMessage GetEmployeeAvgAge(int EmployeeId)
        {
            try
            {
                var result = new HttpResponseMessage(HttpStatusCode.OK);

                var member = db.Employees.Where(x => x.EmployeeId == EmployeeId).FirstOrDefault();

                if (member != null)
                {
                    var data = db.Employees.Select(x => x.EmployeeAge).ToList();
                    double age = 0;
                    age = data.AsQueryable().Sum();
                    age = Math.Ceiling(age / data.Count());

                    if (member.EmployeeAge > age)
                    {

                        var obj = new
                        {
                            Message = "Employee is above average age",
                            EmployeeName = member.EmployeeName,
                            EmployeeAge = member.EmployeeAge,
                            EmployeeID = member.EmployeeId,
                            EmployeeSalary = member.EmployeeSalary,
                            AverageAge = age
                        };
                        result.Content = new StringContent(JsonConvert.SerializeObject(obj));
                    }
                    else
                    {
                        var obj = new
                        {
                            Message = "Employee is below average age",
                            EmployeeName = member.EmployeeName,
                            EmployeeAge = member.EmployeeAge,
                            EmployeeID = member.EmployeeId,
                            EmployeeSalary = member.EmployeeSalary,
                            AverageAge = age
                        };
                        result.Content = new StringContent(JsonConvert.SerializeObject(obj));
                    }
                   
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                }
                else
                {
                    var obj = new
                    {
                        Message = "Employee not found",
                        
                    };
                    result.Content = new StringContent(JsonConvert.SerializeObject(obj));

                }


                return result;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }


        [HttpGet]
        public HttpResponseMessage GetEmployeeAvgSalary(int EmployeeId)
        {
            try
            {
                var result = new HttpResponseMessage(HttpStatusCode.OK);

                var member = db.Employees.Where(x => x.EmployeeId == EmployeeId).FirstOrDefault();

                if (member != null)
                {
                    var data = db.Employees.Select(x => x.EmployeeSalary).ToList();
                    double salary = 0;
                    salary = data.AsQueryable().Sum();
                    salary = Math.Ceiling(salary / data.Count());

                    if (member.EmployeeSalary > salary)
                    {

                        var obj = new
                        {
                            Message = "Employee salary is above average",
                            EmployeeName = member.EmployeeName,
                            EmployeeAge = member.EmployeeAge,
                            EmployeeID = member.EmployeeId,
                            EmployeeSalary = member.EmployeeSalary,
                            AverageSalary = salary
                        };
                        result.Content = new StringContent(JsonConvert.SerializeObject(obj));
                    }
                    else
                    {
                        var obj = new
                        {
                            Message = "Employee salary is below average ",
                            EmployeeName = member.EmployeeName,
                            EmployeeAge = member.EmployeeAge,
                            EmployeeID = member.EmployeeId,
                            EmployeeSalary = member.EmployeeSalary,
                            AverageSalary = salary
                        };
                        result.Content = new StringContent(JsonConvert.SerializeObject(obj));
                    }

                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                }
                else
                {
                    var obj = new
                    {
                        Message = "Employee not found",

                    };
                    result.Content = new StringContent(JsonConvert.SerializeObject(obj));

                }


                return result;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
