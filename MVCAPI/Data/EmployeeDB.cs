﻿using MVCAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MVCAPI.Data
{
    public class EmployeeDB: DbContext
    {
        public EmployeeDB() : base("DefaultConnection")
        {

        }

        public DbSet<Employee> Employees { get; set; }
    }
}