﻿using MVCAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCAPI.Data
{
    public class Dummy
    {
        public static List<Employee> GetEmployees(EmployeeDB context)
        {
            List<Employee> employees = new List<Employee>()
            {
            new Employee()
            {
                EmployeeName = "John",
                EmployeeAge = 30,
                EmployeeSalary = 25000
            },
            new Employee()
            {
                 EmployeeName = "Daniel",
                 EmployeeAge = 40,
                EmployeeSalary = 35000
            }
            };

            return employees;
        }
    }
}