﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MVCAPI.Models
{
    public class Employee 
    {

       
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public byte[] EmployeeImage { get; set; }
        public int EmployeeSalary { get; set; }
        public int EmployeeAge { get; set; }
    }

   
}